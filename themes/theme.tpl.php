<!doctype html>
<html>
	<head>
		<script src="assets/js/phaser.min.js"></script>
		<!--<script src="assets/js/phaser-capture.js"></script>-->
		<script src="assets/js/Blob.js"></script>
		<script src="assets/js/canvas-toBlob.js"></script>
		<script src="assets/js/FileSaver.js"></script>
		
	</head>
	<body>
		<style>
			body{
				margin: 0px;
				padding: 0px;
				background-color: #504e50;
			}
		</style>
		<script>

			//http://phaser.io/examples/v2/buttons/button-in-a-group
			//https://github.com/spite/ccapture.js -> RECORD CANVAS 2 VIDEO
			//https://github.com/niklasvh/html2canvas -> CANVAS 2 IMAGE
			
			//https://github.com/noffle/phaser-capture
			//https://github.com/eligrey/FileSaver.js

			var GAME_WIDTH 	= 1200;
			var GAME_HEIGHT = 848;

			var game;
			var field;
			var layer_1;
			var layer_state = 0;
			var scale = 1;
			var SpriteName;

			var playerFrames = 15; //Begins in zero
			var gkeeperFrames = 15;
			var elementsFrames = 18;

			var nShots = 0;

			var playerBox;

			var nplayer = 0;

			var players = [];

			window.onload = function(){
				game = new Phaser.Game(GAME_WIDTH, GAME_HEIGHT, Phaser.CANVAS, 'FutsalCoach', {preload: preload, create: create, update: update, render: render, init: init});
			}

			function init(){

				game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
				game.scale.pageAlignVertically = true;
				game.scale.pageAlignHorizontally = true;
			}

			function preload(){
				
				game.load.spritesheet('btn_fields', 'assets/data/buttons/btn_field.jpg', 150, 150);
				game.load.spritesheet('btn_players', 'assets/data/buttons/btn_players.jpg', 150, 150);
				game.load.spritesheet('btn_goalkeepers', 'assets/data/buttons/btn_goalkeeper.jpg', 150, 150);
				game.load.spritesheet('btn_elements', 'assets/data/buttons/btn_elements.jpg', 150, 150);
				game.load.spritesheet('btn_arrows', 'assets/data/buttons/btn_elements.jpg', 142, 142);
				game.load.spritesheet('btn_garbage', 'assets/data/buttons/btn_garbage.png', 75, 75);
				game.load.spritesheet('btn_shot', 'assets/data/buttons/btn_shot.jpg', 150, 150);
				game.load.spritesheet('btn_close', 'assets/data/buttons/btn-close.png', 38, 36);
				game.load.spritesheet('btn_field_1', 'assets/data/buttons/field_1.jpg', 150, 150);
				game.load.spritesheet('btn_field_2', 'assets/data/buttons/field_2.jpg', 150, 150);
				game.load.spritesheet('btn_field_3', 'assets/data/buttons/field_3.jpg', 150, 150);
				game.load.spritesheet('btn_field_4', 'assets/data/buttons/field_4.jpg', 150, 150);
				game.load.spritesheet('playerB', 'assets/data/players/sprite/playerB.png', 142, 142, playerFrames);
				game.load.spritesheet('playerY', 'assets/data/players/sprite/playerY.png', 142, 142, playerFrames);
				game.load.spritesheet('playerY', 'assets/data/players/sprite/playerR.png', 142, 142, playerFrames);
				game.load.spritesheet('goalkeepers', 'assets/data/goalkeepers/sprite/goalKeepers.png', 142, 142, gkeeperFrames);
				game.load.spritesheet('elements', 'assets/data/elements/sprites/elements_training.png', 142, 142, elementsFrames);
				

				game.load.image('field_1', 'assets/data/field/field_1.jpg');
				game.load.image('field_2', 'assets/data/field/field_2.jpg');
				game.load.image('field_3', 'assets/data/field/field_3.jpg');
				game.load.image('field_4', 'assets/data/field/field_4.jpg');
				game.load.image('layer', 'assets/data/layers/layer_1.png');
			}

			function create(){

				field = game.add.sprite(game.world.centerX, game.world.centerY, 'field_1');
				field.anchor.setTo(0.5,0.5);

				putMenu();
			}

			function putMenu(){

				btn_1 = game.add.button(10, 730, 'btn_fields', function(){
					if(btn_1.enable == true) showBtnFields();
				}, this, 1);

				btn_2 = game.add.button(170, 730, 'btn_players', function(){
					if(btn_2.enable == true) showBtnPlayers();
				}, this, 1);

				btn_3 = game.add.button(330, 730, 'btn_goalkeepers', function(){
					if(btn_3.enable == true) showBtnGkeeper();
				}, this, 1);

				btn_4 = game.add.button(490, 730, 'btn_elements', function(){
					if(btn_4.enable == true) showBtnElements(); 
				}, this, 1);

				btn_5 = game.add.button(650, 730, 'btn_arrows', function(){
					game.input.onDown.add(putArrows, this);
				}, this, 1);

				garbage = game.add.button(1100, 760, 'btn_garbage', function(){
					if(garbage.enable == true){
						//showBtnElements();
					}
				}, this, 1);

				btn_shot = game.add.button(1030, 10, 'btn_shot', function(){
					if(btn_shot.enable == true){
						hiddenMenu(0);
						setTimeout(function(){ 
							shotCanvas();
							hiddenMenu(1);
						}, 500);
					}
				}, this, 1);

				garbage.input.pixelPerfectOver = true;
				garbage.tint = "0xffffff";

				enableMenu(true);

			}

			var circle, myCircle, line1, startArrow = false;

			function putArrows(){

				//  And display our circle on the top
				circle = new Phaser.Circle(game.world.centerX, game.world.centerY, 10);

				graphics = game.add.graphics(0, 0);
				graphics.lineStyle(2, 0x00ff00, 1);

				myCircle = graphics.drawCircle(game.input.mousePointer.x, game.input.mousePointer.y, circle.diameter);
				
				//line1 = new Phaser.Line(circle.x, circle.y, game.input.mousePointer.x, game.input.mousePointer.y);
				line1 = new Phaser.Line(100, 100, 200, 200);

				startArrow = true;

			}

			function enableMenu(status){

				var btns = [btn_1, btn_2, btn_3, btn_4, btn_5, btn_shot];

				for(var btn in btns){
					btns[btn].enable = status;
				}
			}

			function hiddenMenu(menu_status){

				var btns = [btn_1, btn_2, btn_3, btn_4, btn_5, btn_shot, garbage];

				for(var btn in btns){
					btns[btn].alpha = menu_status;
				}

				return true;
			}

			function putLayer(destroyFunc){

				layer_1 = game.add.sprite(game.world.centerX, game.world.centerY, 'layer');
				layer_1.anchor.setTo(0.5,0.5);

				game.add.tween(layer_1).to( { alpha: 1 }, 300, "Linear", true, 100);

				btn_close = game.add.button(1100, 50, 'btn_close', function(){
					destroyLayer();
					destroyFunc();
				}, this, 1);

				enableMenu(false);
			}

			function destroyLayer(){
				layer_1.destroy();
				btn_close.destroy();
				enableMenu(true);
			}

			function showBtnPlayers () {

				putLayer(destroyPlayers);

				var posx = 190;
				var posy = 120;

				playerBtn = [];

				SpriteName = 'playerB';

				for(x = 0; x <= playerFrames; x++){

					playerBtn[x] = game.add.sprite(posx, posy, SpriteName, x);
					playerBtn[x].anchor.setTo(0.5, 0.5);
					playerBtn[x].alpha = 0;

			    	game.add.tween(playerBtn[x]).to( { alpha: 1 }, 500, "Linear", true, 200);

			    	playerBtn[x].inputEnabled = true;
					
					playerBtn[x].events.onInputOver.add(function(){
						this.game.canvas.style.cursor = "pointer";
						//game.physics.arcade.moveToPointer(playerBtn[x], 300);
					}, this);

					playerBtn[x].events.onInputOut.add(function(){
						this.game.canvas.style.cursor = "default";
					}, this);

			    	playerBtn[x].events.onInputDown.add(putSprite, this);

			    	playerBtn[x].framePos = x;

					posx = posx + 200;

					if(posx > 1000){
						posx = 190;
						posy = posy + 150;
					}

			    }

			}

			function showBtnGkeeper () {

				putLayer(destroyPlayers);

				var posx = 190;
				var posy = 120;

				playerBtn = [];

				SpriteName = 'goalkeepers';

				for(x = 0; x <= gkeeperFrames; x++){

					playerBtn[x] = game.add.sprite(posx, posy, SpriteName, x);

					playerBtn[x].anchor.setTo(0.5, 0.5);

					playerBtn[x].alpha = 0;

			    	game.add.tween(playerBtn[x]).to( { alpha: 1 }, 200, "Linear", true, 200);

			    	playerBtn[x].inputEnabled = true;
					
					playerBtn[x].events.onInputOver.add(function(){
						this.game.canvas.style.cursor = "pointer";
					}, this);

					playerBtn[x].events.onInputOut.add(function(){
						this.game.canvas.style.cursor = "default";
					}, this);

			    	playerBtn[x].events.onInputDown.add(putSprite, this);

			    	playerBtn[x].framePos = x;

					posx = posx + 200;

					if(posx > 1000){
						posx = 190;
						posy = posy + 150;
					}

			    }
			}

			function showBtnElements () {

				putLayer(destroyPlayers);

				var posx = 190;
				var posy = 120;

				playerBtn = [];

				SpriteName = 'elements';

				for(x = 0; x <= elementsFrames; x++){

					playerBtn[x] = game.add.sprite(posx, posy, SpriteName, x);

					playerBtn[x].anchor.setTo(0.5, 0.5);

					playerBtn[x].alpha = 0;

			    	game.add.tween(playerBtn[x]).to( { alpha: 1 }, 200, "Linear", true, 200);

			    	playerBtn[x].inputEnabled = true;
					
					playerBtn[x].events.onInputOver.add(function(){
						this.game.canvas.style.cursor = "pointer";
					}, this);

					playerBtn[x].events.onInputOut.add(function(){
						this.game.canvas.style.cursor = "default";
					}, this);

			    	playerBtn[x].events.onInputDown.add(putSprite, this);

			    	playerBtn[x].framePos = x;

					posx = posx + 200;

					if(posx > 1000){
						posx = 190;
						posy = posy + 150;
					}

			    }
			}

			function putSprite(element){
				
				nplayer++;

				//console.log(player.framePos);
				players[nplayer] = game.add.sprite(150, 150, SpriteName, element.framePos);

				players[nplayer].anchor.set(-2.5, 0.7); //centro del campo principal

				game.add.tween(players[nplayer]).to( { y: game.world.centerY }, 600, Phaser.Easing.Bounce.Out, true);

				players[nplayer].inputEnabled = true;
				players[nplayer].pixelPerfectOver = true;

				players[nplayer].events.onInputOver.add(function(sprite){
					this.game.canvas.style.cursor = "move";
					sprite.input.enableDrag(false, true);
					active_player = sprite;
				}, this);
				
				players[nplayer].events.onInputOut.add(function(){
					this.game.canvas.style.cursor = "default";
					//players[nplayer].input.enableDrag(false);
				}, this);
				
				players[nplayer].events.onDragUpdate.add(function(sprite){
					isOverGarbage(sprite);
					sprite.alpha = 0.8;
				},this);
					
				players[nplayer].events.onDragStop.add(function(sprite){
					checkOverlap(sprite, garbage);
					sprite.alpha = 1;
				},this);

				players[nplayer].framePos = element.framePos;

				destroyPlayers();
				destroyLayer();
			}

			
			var destroyPlayers = function(){
				for(x = 0; x < playerBtn.length; x++){
					playerBtn[x].destroy();
				}
			}

			function isOverGarbage(spriteA) {

			    var boundsA = spriteA.getBounds();
			    var boundsB = garbage.getBounds();

			    if(Phaser.Rectangle.intersects(boundsA, boundsB) == true){

			    	var tween = game.add.tween(garbage).to( { y: [780, 760] }, 1500, Phaser.Easing.Bounce.Out, true);

			    	tween.onStart.add(function(){
			    		garbage.tint = "0xcb2222";
			    	}, this);

			    	tween.onComplete.add(function(){
			    		garbage.tint = "0xffffff";
			    	}, this);
			    }

			}

			function checkOverlap(spriteA) {

			    var boundsA = spriteA.getBounds();
			    var boundsB = garbage.getBounds();

			    if(Phaser.Rectangle.intersects(boundsA, boundsB) == true){
			    	spriteA.destroy();
			    }

			}

			function showBtnFields () {

				putLayer(destroyFields);

				text = game.add.text(game.world.centerX, game.world.centerY, "- Selecciona un campo - ", {
				    font: "65px Arial",
				    fill: "#fff",
				    align: "center"
				});

				text.anchor.setTo(0.5, 0.5);

				btn_1 = game.add.button(120, 100, 'btn_field_1', setField, this, 2, 1, 0);
				btn_1.alpha = 0;
				btn_1.name = 'field_1';

				btn_2 = game.add.button(320, 100, 'btn_field_2', setField, this, 2, 1, 0);
				btn_2.alpha = 0;
				btn_2.name = 'field_2';

				btn_3 = game.add.button(520, 100, 'btn_field_3', setField, this, 2, 1, 0);
				btn_3.alpha = 0;
				btn_3.name = 'field_3';

				btn_4 = game.add.button(720, 100, 'btn_field_4', setField, this, 2, 1, 0);
				btn_4.alpha = 0;
				btn_4.name = 'field_4';

				var btns = [btn_1, btn_2, btn_3, btn_4];

				for(btn in btns){
					game.add.tween(btns[btn]).to( { alpha: 1 }, 300, "Linear", true, 600);	
				}

			}

			function setField(button){

				field.loadTexture(button.name, 0, false); //cambiamos la imagen del campo
				game.add.tween(layer_1).to( { alpha: 1 }, 300, "Linear", true, 100);
				
				destroyFields();
				destroyLayer();
			}

			function destroyFields(){

				var btns = [btn_1, btn_2, btn_3, btn_4, text];

				for(btn in btns){
					btns[btn].destroy();
				}
			}

			function update() {

				if (game.input.keyboard.isDown(Phaser.Keyboard.UP)){
					console.log("UP BTN");
				}

				if (game.input.keyboard.isDown(Phaser.Keyboard.DOWN)){
					console.log("DOWN BTN");
				}

				if (game.input.keyboard.isDown(Phaser.Keyboard.LEFT)){
					console.log("LEFT BTN");
				}

				if(startArrow == true){
					//console.log(myCircle);
					line1.start.x = garbage.position.x;
					line1.start.y = garbage.position.y;
					line1.end.x = game.input.mousePointer.x;
					line1.end.y = game.input.mousePointer.y
				}

			}

			function shotCanvas(){

				var today = new Date();

				h = today.getHours();
				i = today.getMinutes();
				s = today.getSeconds();
				d = today.getUTCDate();
				m = today.getUTCMonth();
				Y = today.getFullYear();

				var str_time = Y + "-" + m + "-" + d +"_" + h + "-" + i + "-" + s;

				/*
				//Se puede intentar guardar en el servidor enviando la imagen por url 
				game.capture = game.plugins.add(Phaser.Plugin.Capture);
				game.capture.screenshot(function(dataUrl) {
					var img = document.getElementById("image_download");
					img.src = dataUrl;
					window.location.href = img.src.replace('image/png', 'image/octet-stream');
				});
				*/
			
				var futsalCanvas = document.getElementsByTagName("CANVAS")[0];
				futsalCanvas.id = "my-canvas"

				var canvas = document.getElementById("my-canvas"), ctx = canvas.getContext("2d");
				// draw to canvas...
				canvas.toBlob(function(blob) {
					nShots++;
				    saveAs(blob, "futsalCoach-shot-" + str_time + "_" + ("0"+nShots).slice(-2) + ".png");
				});

			}


			function render(){
				game.debug.geom(line1);
			}

		</script>
		<img src="" style="display: none;" id="image_download" />
	</body>
</html>